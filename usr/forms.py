from allauth.account.forms import LoginForm, SignupForm, ResetPasswordForm, ResetPasswordKeyForm, ChangePasswordForm, SetPasswordForm
from allauth.socialaccount.forms import SignupForm as SocialSignupForm
from django import forms


class LoginForm(LoginForm):

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['login'].widget = forms.TextInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': 'Ваш логин'
        })
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': 'Ваш пароль'
        })


class SignupForm(SignupForm):

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш логин'
        })
        self.fields['email'].widget = forms.EmailInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш Email'
        })
        self.fields['password1'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш пароль'
        })
        self.fields['password2'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш пароль еще раз'
        })

class SocialSignupForm(SocialSignupForm):
    def __init__(self, *args, **kwargs):
        super(SocialSignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш логин'
        })
        self.fields['email'].widget = forms.EmailInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш Email'
        })


class ResetPasswordForm(ResetPasswordForm):

    def __init__(self, *args, **kwargs):
        super(ResetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget = forms.EmailInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш Email'
        })


class ResetPasswordKeyForm(ResetPasswordKeyForm):

    def __init__(self, *args, **kwargs):
        super(ResetPasswordKeyForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш пароль'
        })
        self.fields['password2'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш пароль еще раз'
        })


class ChangePasswordForm(ChangePasswordForm):

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['oldpassword'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш старый пароль'
        })
        self.fields['password1'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш новый пароль'
        })
        self.fields['password2'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш новый пароль еще раз'
        })

class SetPasswordForm(SetPasswordForm):

    def __init__(self, *args, **kwargs):
        super(SetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш пароль'
        })
        self.fields['password2'].widget = forms.PasswordInput(attrs={
            'class': 'form-control',
            'autofocus': False,
            'placeholder': u'Ваш пароль еще раз'
        })