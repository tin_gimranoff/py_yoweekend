from time import sleep

from datetime import datetime
from django.core.management.base import BaseCommand

from event.models import Event
from yoweekend.settings import VK_LOGIN, VK_PASSWORD, MONTH_PAGES, VK_GROUP_ID
import vk_api
import locale


class Command(BaseCommand):
    def handle(self, *args, **options):
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        login, password = VK_LOGIN, VK_PASSWORD
        group_id = VK_GROUP_ID
        i = int(datetime.today().strftime('%m'))-1
        vk_session = vk_api.VkApi(login, password)
        try:
            vk_session.auth()
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return
        vk = vk_session.get_api()
        while i < len(MONTH_PAGES):
            current_month = i + 1
            events = Event.objects.extra(where={
                'MONTH(meeting_date) = {0} AND YEAR(meeting_date) = 2018'.format(current_month)
            }).order_by('meeting_date', 'time_begin').all()
            event_objects = []
            for e in events:
                event_objects.append("{0}'''{1} {2}'''\n{3}\n[{4}|{4}]\n\n".format(
                    MONTH_PAGES[i]['smile'],
                    e.meeting_date.strftime("%d %B %Y"),
                    e.time_begin.strftime("%H:%M") if e.no_time == False else '',
                    e.name,
                    e.link or 'http://yoweekend.ru/events/event_%s/' % (e.id)
                ))
            pages = []
            pages.append('')
            counter_page = 0
            for obj in event_objects:
                if len(pages[counter_page]+obj) > 10000:
                    counter_page += 1
                    pages.append('')
                pages[counter_page] += obj
            print(len(pages))
            if len(pages) == 1:
                response = vk.pages.save(text=pages[0], page_id=MONTH_PAGES[i]['page_id'], group_id=group_id, title=MONTH_PAGES[i]['title'])
                print(response)
            else:
                pagination = []
                page_ids = []
                for j, p in enumerate(pages):
                    if j == 0:
                        pagination.append("<b><i>Страницы:</i></b> [[page-%s_%s|%s]]" % (group_id, MONTH_PAGES[i]['page_id'], j+1))
                        page_ids.append(MONTH_PAGES[i]['page_id'])
                    else:
                        response = vk.pages.save(text='', group_id=group_id, title="%s Страница %s" % (MONTH_PAGES[i]['title'], j+1))
                        pagination.append('[[page-%s_%s|%s]]' % (group_id, response, j + 1))
                        page_ids.append(response)
                for j, p in enumerate(pages):
                    text = ' '.join(pagination)+"\n\n"+p+"\n\n"+' '.join(pagination)
                    response = vk.pages.save(text=text, page_id=page_ids[j], group_id=group_id)
                    print(response)
            i += 1
            sleep(20)
