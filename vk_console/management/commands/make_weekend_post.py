from django.core.management.base import BaseCommand
from datetime import datetime
from event.models import Event
from yoweekend.settings import VK_LOGIN, VK_PASSWORD, VK_GROUP_ID, VK_GROUP_AlBUM_ID, VK_SMILE
import vk_api
import locale


class Command(BaseCommand):
    def handle(self, *args, **options):
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        login, password = VK_LOGIN, VK_PASSWORD
        group_id = VK_GROUP_ID
        album_id = VK_GROUP_AlBUM_ID
        d_today = datetime.today()
        events = Event.objects.filter(meeting_date=d_today).order_by('time_begin')
        if len(events) > 0:
            vk_session = vk_api.VkApi(login, password)
            try:
                vk_session.auth()
                upload = vk_api.VkUpload(vk_session)
            except vk_api.AuthError as error_msg:
                print(error_msg)
                return
        else:
            return
        message = "Сегодня %s, а значит нас ждет насыщенная программа 🔥🔥🔥\n\n" % d_today.strftime('%A')
        message += "Мы составили список мероприятий, чтобы тебе было удобнее😏 \nВыбирай:\n\n"
        count_images = 0
        attachments = []
        for e in events:
            hash_tags = [
                '#обнинск',
                '#событияобнинск',
                '#obnx',
                '#yoweekend',
                '#вечеринка',
                '#афиша',
                '#кудасходить',
                '#'+d_today.strftime('%A')
            ]
            if e.link:
                link = e.link
            else:
                link = "http://yoweekend.ru/events/event_%s/" % e.id

            if e.no_time:
                time = ''
            else:
                time = ' '+e.time_begin.strftime("%H:%M")
            message += "%s%s %s (%s)\n" % (VK_SMILE, time, e.name, link)
            if count_images < 10:
                photo = upload.photo(  # Подставьте свои данные
                    e.image_original[1:],
                    album_id=album_id,
                    group_id=group_id
                )
                attachments.append('photo%s_%s' % (photo[0]['owner_id'], photo[0]['id']))
                count_images = count_images + 1
        message += "\n\n\n\n_____________\n"
        message += "💡Если ты любишь продумывать свой досуг заранее, то на нашем сайте http://yoweekend.ru можно найти все предстоящие мероприятия и события в нашем городе!\n"
        message += "Так же добавляй свои мероприятия на этот сайт или в этой группе, просто написав сообщение.\n"
        message = message + "\n\n\n" + ' '.join(hash_tags)
        vk = vk_session.get_api()
        print(attachments)
        response = vk.wall.post(owner_id='-%s' % group_id, from_group=1, message=message, attachments=','.join(attachments))
        print(response)
        vk.wall.pin(owner_id='-%s' % group_id, post_id=response['post_id'])
