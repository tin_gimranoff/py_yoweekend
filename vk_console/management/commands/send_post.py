from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from django.template.defaultfilters import truncatechars
from event.models import Event
from yoweekend.settings import VK_LOGIN, VK_PASSWORD, VK_GROUP_ID, VK_GROUP_AlBUM_ID
import vk_api
import locale


class Command(BaseCommand):
    def handle(self, *args, **options):
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        login, password = VK_LOGIN, VK_PASSWORD
        group_id = VK_GROUP_ID
        album_id = VK_GROUP_AlBUM_ID
        current_hours = datetime.today().strftime('%H')
        d_tomorrow = datetime.today() + timedelta(days=1)
        d_today = datetime.today()
        if int(current_hours) < 15:
            events = Event.objects.filter(isVkSend=0, meeting_date=d_today, isDraft=0).order_by('meeting_date')[:5]
        else:
            events = Event.objects.filter(isVkSend=0, isDraft=0)
            events = events.extra(where=[
                'meeting_date = "{0}" OR meeting_date = "{1}"'.format(
                    d_today.strftime('%Y-%m-%d'),
                    d_tomorrow.strftime('%Y-%m-%d')
                )
            ])
            events = events.order_by('meeting_date')[:5]
        if len(events) > 0:
            vk_session = vk_api.VkApi(login, password)
            try:
                vk_session.auth()
            except vk_api.AuthError as error_msg:
                print(error_msg)
                return
        else:
            return
        for e in events:
            hash_tags = ['#обнинск', '#событияобнинск', '#obnx', '#yoweekend']
            for category in e.categories.all():
                hash_tags.append('#'+category.name.replace(' ', '_').replace(',', '').lower())
            message = e.get_all_dates_string() + " состоится \n"
            message = message + e.name + "\n\n"
            message = message + truncatechars(e.description, 300) + "\n\n"
            if e.link is not None:
                message = message + "Подробнее по ссылке: " + e.link + "\n\n"
            else:
                link = "http://yoweekend.ru/events/event_%s/" % e.id
                message = message + "Подробнее по ссылке: " + link + "\n\n"
            message = message + "______________\nБольше мероприятий и событий на сайте http://yoweekend.ru! Так же " \
                                "добавляй свои мероприятия на этот сайт или в этой группе, просто написав сообщение.";
            message = message + "\n\n\n" + ' '.join(hash_tags)
            upload = vk_api.VkUpload(vk_session)

            photo = upload.photo(  # Подставьте свои данные
                e.image_original[1:],
                album_id=album_id,
                group_id=group_id
            )
            vk = vk_session.get_api()
            response = vk.wall.post(owner_id='-%s' % group_id, from_group=1, message=message,
                                    attachments='photo%s_%s' % (photo[0]['owner_id'], photo[0]['id']))
            e.isVkSend = 1
            e.save()
