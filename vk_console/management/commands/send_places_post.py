from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from event.models import Event
from place.models import Place
from yoweekend.settings import VK_LOGIN, VK_PASSWORD, VK_GROUP_ID, VK_GROUP_PLACE_AlBUM_ID, VK_SMILE
import vk_api
import locale


class Command(BaseCommand):
    def handle(self, *args, **options):
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        login, password = VK_LOGIN, VK_PASSWORD
        group_id = VK_GROUP_ID
        album_id = VK_GROUP_PLACE_AlBUM_ID

        d_tomorrow = datetime.today() + timedelta(days=1)
        d_today = datetime.today()

        events = Event.objects.filter(isVkSend=0, isDraft=0)
        events = events.extra(where=[
            'meeting_date = "{0}" OR meeting_date = "{1}"'.format(
                d_today.strftime('%Y-%m-%d'),
                d_tomorrow.strftime('%Y-%m-%d')
            )
        ]).all()

        if len(events) > 0:
            return

        places = Place.objects.filter(isDraft=0, isVkSend=0).order_by('create_date')[:10]
        if len(places) > 0:
            vk_session = vk_api.VkApi(login, password)
            try:
                vk_session.auth()
                upload = vk_api.VkUpload(vk_session)
            except vk_api.AuthError as error_msg:
                print(error_msg)
                return
        else:
            return

        message = "Очередная подборка последних добавленных на сайт 10 развлекательных мест нашего города.🔥🔥🔥\n"
        message += "Выбирай, где ты хочешь провести свой досуг😉😉😉:\n\n"
        count_images = 0
        attachments = []
        hash_tags = ['#обнинск', '#места', '#obnx', '#yoweekend', '#relax', '#гдеотдохнуть']
        for p in places:
            for category in p.categories.all():
                hash_tags.append('#'+category.name.replace(' ', '_').replace(',', '').lower())
            link = p.link if p.link else "http://yoweekend.ru/places/place_%s/" % p.id
            message += "%s%s (%s)\n" % (VK_SMILE, p.name, link)
            if count_images < 10:
                photo = upload.photo(  # Подставьте свои данные
                    p.image_original[1:],
                    album_id=album_id,
                    group_id=group_id
                )
                attachments.append('photo%s_%s' % (photo[0]['owner_id'], photo[0]['id']))
                count_images = count_images + 1
            p.isVkSend = 1
            p.save()
        message += "\nПолный список мест можно найти по ссылке: http://yoweekend.ru/places/"
        message += "\n\n\n\n_____________\n"
        message += "💡Все предстоящие мероприятия и развлекательные места города в одном месте http://yoweekend.ru.\n"
        message += "Регистрируйся на сайте и добавляй мероприятия и места самостоятельно!\n"
        message += "Планируй свой досуг правильно - вместе с нами!\n"
        message = message + "\n\n\n" + ' '.join(list(set(hash_tags)))
        vk = vk_session.get_api()
        response = vk.wall.post(owner_id='-%s' % group_id, from_group=1, message=message,
                                attachments=','.join(attachments))