# -*- coding: UTF-8 -*-

import os

import re
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse, Http404
from django.shortcuts import redirect, render
from django.template.loader import get_template
from django.utils.html import linebreaks
from django.views.decorators.csrf import csrf_exempt

from app.views import getPreviewSize
from city.models import City
from place.forms import AddPlaceForm
from place.models import Place, PlaceCategory
from yoweekend.settings import BASE_DIR, EVENTS_PER_PAGE
from django.db.models.functions import datetime
from time import time
from PIL import Image


def index(request, id=None, category_alias=None):
    category = None
    city = City.detect_city(City, request=request)
    places = Place.objects.filter(city_id=city.id, isDraft=0)
    if category_alias is not None:
        places = places.filter(categories__alias=category_alias)
        try:
            category = PlaceCategory.objects.get(alias=category_alias)
        except PlaceCategory.DoesNotExist:
            raise Http404("Категория не найдена")
    places = places.order_by('name').all()[:EVENTS_PER_PAGE]
    place_info = None
    place_info_text = None
    if id is not None:
        place_info = Place.objects.get(id=id)
        if place_info:
            place_info_text = re.sub(
                r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w_-]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)',
                r'<a href="\1" target="_blank">\1</a>', place_info.description, flags=re.IGNORECASE)
            place_info_text = re.sub(r'<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"', r'<a href="mailto:\1"',
                                     place_info_text)
            place_info_text = linebreaks(place_info_text)

    #Определяем метатеги
    meta_tags = {}
    if category_alias is None and id is None:
        meta_tags['title'] = u'Все развлекательные места вашего города — %s — YOweekend.ru' % city.name
        meta_tags['description'] = u'Все развлекательные места и заведения в городе %s' % city.name
        meta_tags['keywords'] = u'развелечения в городе, развелкательные места, места, досуг, куда сходить, %s' % city.name

    if category_alias is not None:
        meta_tags['title'] = u'%s — Все места развлечений вашего города — %s — YOweekend.ru' % (category.name, city.name)
        if category.meta_description is not None:
            meta_tags['description'] = category.meta_description % (city.name)
        else:
            meta_tags['description'] = u'Все места развлечений в городе %s в категории %s' % (city.name, category.name)
        if category.meta_keywords is not None:
            meta_tags['keywords'] = category.meta_keywords % (city.name)
        else:
            meta_tags['keywords'] = u'развелечения в городе, развелкательные места, места, досуг, куда сходить, %s, %s' % (city.name, category.name)

    if id is not None:
        meta_tags['title'] = u'%s — Все места развлечений вашего города — %s — YOweekend.ru' % (place_info.name, city.name)
        meta_tags['description'] = place_info.description[:100]
        meta_tags['keywords'] = place_info.name

    response = render(request, 'place/index.html', {
        'city': city,
        'places': places,
        'category': category,
        'place_info': place_info,
        'place_info_text': place_info_text,
        'meta_tags': meta_tags
    })
    return response


def add(request, id=None):
    if not request.user.is_authenticated:
        return redirect('/')
    if id is None:
        title = u'Новое место'
    else:
        title = u'Редактирование места'
    flash_message = {}
    model = None
    if request.method == 'POST':
        user = User.objects.get(id=request.user.id)
        if id is not None:
            try:
                model = Place.objects.get(id=id, user_id=user.id)
            except:
                return redirect('/')
            form = AddPlaceForm(request.POST or None, instance=model)
        else:
            form = AddPlaceForm(request.POST or None)
        if form.is_valid():
            place_cont = form.save(commit=False)
            # Создаем папку для изображений
            directory = '/static/media/userimages/%s/' % user.id
            if not os.path.exists(BASE_DIR + directory):
                os.makedirs(BASE_DIR + directory)
            # Копируем основное изображение из временной папки
            source_filename, file_extension = os.path.splitext(form.cleaned_data['image_original'])
            _new_file_name = hex(int(time() * 10000000))[2:]
            _main_image = Image.open(BASE_DIR + form.cleaned_data['image_original'])
            _main_image.save(BASE_DIR + directory + _new_file_name + file_extension)
            place_cont.image_original = directory + _new_file_name + file_extension
            # КОНЕЦ скопировали основное изображение
            # Создаем превью 300 на 300 из превью, которое пришло из формы
            x1 = form.cleaned_data['x1']
            y1 = form.cleaned_data['y1']
            _preview_image = Image.open(BASE_DIR + form.cleaned_data['image_preview'])
            _preview_image = _preview_image.crop((x1, y1, x1 + 300, y1 + 300))
            _preview_image.save(BASE_DIR + directory + 'preview_' + _new_file_name + file_extension)
            place_cont.image_preview = directory + 'preview_' + _new_file_name + file_extension
            # КОНЕЦ сделали превью и скопировали его в нужную папку
            place_cont.user = user
            if place_cont.id is None:
                place_cont.create_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            else:
                place_cont.update_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            place_cont.save()
            place_cont.categories.clear()
            for c in form.cleaned_data['categories']:
                place_cont.categories.add(c)
            messages.add_message(request, messages.SUCCESS,
                                 'Ваше место успешно сохранено, теперь его видят пользователи в ленте.',
                                 extra_tags='SAVE_SUCCESS')
            return redirect('/places/edit/%s/' % place_cont.id)
        else:
            flash_message = {
                'class': 'warning',
                'title': 'Ошибка',
                'message': 'В форме есть ошибки. Исправьте их и повторите сохранение',
            }
    else:
        if id is None:
            form = AddPlaceForm(initial={
                'image_preview': '/static/img/temp_image.png',
                'image_original': '/static/img/temp_image.png',
                'x1': 0,
                'x2': 299,
                'y1': 0,
                'y2': 299,
            })
        else:
            try:
                model = Place.objects.get(id=id, user_id=request.user.id)
            except:
                return redirect('/')
            form = AddPlaceForm(initial={
                'x1': 0,
                'x2': 299,
                'y1': 0,
                'y2': 299,
            }, instance=model)
    return render(request, 'place/add.html', {
        'form': form,
        'flash_message': flash_message,
        'title': title,
        'model': model
    })


def upload_image(request):
    if request.is_ajax() and request.FILES['place_image']:
        image_file = request.FILES['place_image']
        source_filename, file_extension = os.path.splitext(image_file.name)
        if file_extension not in ['.jpg', '.png', '.jpeg']:
            return JsonResponse({
                'status': False,
                'textError': u'Вы можете загружать только файлы изображений с разрешениями png, jpg, jpeg.'
            })
        fs = FileSystemStorage()
        directory = '/static/media/temp/%s/' % (request.session.session_key)
        if not os.path.exists(BASE_DIR + directory):
            os.makedirs(BASE_DIR + directory)
        uid = hex(int(time() * 10000000))[2:]
        target_file_name = uid + file_extension
        target_uploaded_file_name = fs.save(BASE_DIR + directory + target_file_name, image_file)
        target_uploaded_file_path = fs.url(target_uploaded_file_name)
        if target_uploaded_file_path:
            source_image = Image.open('/' + target_uploaded_file_path)
            width, height = source_image.size
            options = getPreviewSize(width, height)
            preview_image = source_image.resize((options['width'], options['height']), Image.ANTIALIAS)
            preview_image.save(BASE_DIR + directory + 'preview_' + target_file_name)
            target_uploaded_file_path_preview = directory + 'preview_' + target_file_name
            return JsonResponse({
                'name': target_file_name,
                'size': image_file.size,
                'url': directory + target_file_name,
                'thumbnailUrl': target_uploaded_file_path_preview,
                'width': options['width'],
                'height': options['height'],
            })


def usr_places(request, is_draft=None):
    if not request.user.is_authenticated:
        return redirect('/')
    places = Place.objects.filter(user_id=request.user.id)
    if is_draft != None and is_draft == 'drafts':
        places = places.filter(isDraft=True)
    places = places.order_by('-name').all()

    return render(request, 'place/user_places.html', {
        'places': places,
        'is_draft': is_draft,
    })



@csrf_exempt
def get_place_content(request):
    city = City.detect_city(City, request=request)
    limit = int(request.POST.get('startFrom', 0))
    offset = int(request.POST.get('startFrom', 0)) + int(EVENTS_PER_PAGE)

    places = Place.objects.filter(city_id=city.id, isDraft=0)
    if request.POST.get('category') != 'None':
        places = places.filter(categories__alias=request.POST.get('category'))
    places = places.order_by('name').all()[limit:offset]

    return render(request, 'place/places.html', {
        'from_ajax': True,
        'places': places,
        'city': city,
    })

@csrf_exempt
def get_full_info_modal(request):
    place_id = request.POST.get('id', None)
    if place_id is None:
        pass
    data = Place.objects.get(id=place_id)
    if data is None:
        return JsonResponse({'name': u'Произошла ошибка', 'html': u'Запрашиваемое событие не найдено. Извинине.'})
    html = re.sub(
        r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w_-]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)',
        r'<a href="\1" target="_blank">\1</a>', data.description, flags=re.IGNORECASE)
    html = re.sub(r'<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"', r'<a href="mailto:\1"', html)
    t = get_template('place/_full_info_modal.html')
    html_to_modal = t.render({
        'data': data,
        'text': linebreaks(html),
        'request': request
    })
    return JsonResponse({
        'name': data.name,
        'html': html_to_modal,
    })

@csrf_exempt
def get_full_info(request):
    place_id = request.POST.get('id', None)
    if place_id is None:
        pass
    data = Place.objects.get(id=place_id)
    if data is None:
        return JsonResponse({'name': u'Произошла ошибка', 'html': u'Запрашиваемое событие не найдено. Извинине.'})
    html = re.sub(
        r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w_-]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)',
        r'<a href="\1" target="_blank">\1</a>', data.description, flags=re.IGNORECASE)
    html = re.sub(r'<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"', r'<a href="mailto:\1"', html)
    return JsonResponse({'name': data.name, 'html': linebreaks(html)})


@csrf_exempt
def delete(request):
    if not request.is_ajax():
        return JsonResponse({
            'status': False,
            'error': u'Это не ajax запрос',
        })
    id = request.POST.get('id', None)
    if id is None:
        return JsonResponse({
            'status': False,
            'error': u'Не верный запрос.',
        })
    if request.method == 'POST':
        place = Place.objects.get(id=id)
        if not place:
            return JsonResponse({
                'status': False,
                'error': u'По какой-то причине данного места не существует.',
            })
        if place.user_id != request.user.id:
            return JsonResponse({
                'status': False,
                'error': u'Данное место принадлежит не вам.',
            })
        if place.delete():
            return JsonResponse({
                'status': True,
            })
        else:
            return JsonResponse({
                'status': False,
                'error': u'В процессе удаления произошла ошибка.',
            })


