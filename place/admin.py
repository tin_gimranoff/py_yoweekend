from django.contrib import admin

from place.models import PlaceCategory, Place


class PlaceCategoryAdmin(admin.ModelAdmin):
    def category_place_count(self, obj):
        return obj.place_set.count()
    category_place_count.short_description = u'Количество мест'
    list_display = ('name', 'hidden', 'alias', 'category_place_count', 'meta_keywords')

class PlaceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'isVkSend', 'user')
    list_filter = ('user',)


admin.site.register(PlaceCategory, PlaceCategoryAdmin)
admin.site.register(Place, PlaceAdmin)
