from django.apps import AppConfig

class PlaceConfig(AppConfig):
    name = 'place'
    verbose_name = 'Места'
