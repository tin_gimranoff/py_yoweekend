from django.urls import path
from place import views

urlpatterns = [
    path('', views.index, name='place_list'),
    path('place_<int:id>/', views.index),
    path('add/', views.add, name='add_place'),
    path('edit/<int:id>/', views.add),
    path('upload_image/', views.upload_image),
    path('get_place_content/', views.get_place_content),
    path('get-full-info-modal/', views.get_full_info_modal),
    path('get-full-info/', views.get_full_info),
    path('delete/', views.delete, name='delete_place'),
    path('<str:category_alias>/', views.index),
]
