from django.db import models
from django.contrib.auth.models import User

from city.models import City


class PlaceCategory(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'place_categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('position', 'name')

    name = models.CharField(max_length=255, verbose_name=u'Название', null=False, blank=False, unique=True)
    alias = models.CharField(max_length=255, verbose_name=u'Алиас', null=False, blank=False, unique=True)
    meta_keywords = models.CharField(max_length=255, verbose_name=u'Ключевые слова', null=True, blank=True)
    meta_description = models.CharField(max_length=255, verbose_name=u'Описание', null=True, blank=True)
    position = models.IntegerField(default=0, verbose_name=u'Положение')
    hidden = models.BooleanField(default=False, verbose_name=u"Скрыть категорию")


#Сделать после миграции
#ALTER TABLE `places` CHANGE `description` `description` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
class Place(models.Model):
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'places'
        verbose_name = 'Место'
        verbose_name_plural = 'Места'

    name = models.CharField(max_length=255, verbose_name=u'Название', null=False, blank=False)
    image_original = models.CharField(max_length=255, verbose_name=u'Оригинальное изображение', null=True, blank=True,
                                      default=None)
    image_preview = models.CharField(max_length=255, verbose_name=u'Оригинальное изображение', null=True, blank=True,
                                     default=None)
    link = models.CharField(max_length=255, verbose_name=u'Ссылка на сайт или группу в ВК', null=True, blank=True, default=None)
    city = models.ForeignKey(City, verbose_name=u'Город', on_delete=None, null=False, blank=False, default='')
    categories = models.ManyToManyField(PlaceCategory, verbose_name=u'Категории', default=None)
    address = models.CharField(max_length=255, verbose_name=u'Адрес', null=False, blank=False)
    timing = models.CharField(max_length=255, verbose_name=u'Расписание', null=True, blank=True)
    description = models.TextField(null=True, blank=True, verbose_name=u'Описание', default=None)
    isDraft = models.BooleanField(default=False, verbose_name=u'Черновик')
    isVkSend = models.BooleanField(default=False, verbose_name=u'Статус отправки в группу в ВК')
    user = models.ForeignKey(User, on_delete=None, blank=True)
    create_date = models.DateTimeField(verbose_name=u'Дата создания', default=None, null=True)
    update_date = models.DateTimeField(verbose_name=u'Дата обновления', default=None, null=True)
