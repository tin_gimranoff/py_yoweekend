from place.models import PlaceCategory
from django import template

register = template.Library()


@register.inclusion_tag('categories/_list_in_sidebar_pl.html')
def sidebarlist(request):
    categories = PlaceCategory.objects.order_by('position', 'name').filter(hidden=False).all()
    return {
        'categories': categories,
        'request': request
    }
