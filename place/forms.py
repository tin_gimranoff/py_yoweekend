from django import forms

from place.models import PlaceCategory, Place

from pathlib import Path

from yoweekend.settings import BASE_DIR

CATEGORY_CHOISES = tuple((c.id, c.name) for c in PlaceCategory.objects.order_by('position', 'name').filter(hidden=False).all())

class AddPlaceForm(forms.ModelForm):
    error_messages = {
        'non_exist_file': u'В процессе загрузки файла произошла ошибка',
        'more_categories': u'Можно выбрать не более 3-х категорий',
        'wrong_coords': u'Выбранная область изображения больше чем разрешено. Выделите правильную область.'
    }

    x1 = forms.IntegerField(widget=forms.HiddenInput(attrs={
        'id': 'event-x1'
    }))
    x2 = forms.IntegerField(widget=forms.HiddenInput(attrs={
        'id': 'event-x2'
    }))
    y1 = forms.IntegerField(widget=forms.HiddenInput(attrs={
        'id': 'event-y1'
    }))
    y2 = forms.IntegerField(widget=forms.HiddenInput(attrs={
        'id': 'event-y2'
    }))

    class Meta:
        model = Place
        fields = [
            'image_preview',
            'image_original',
            'name',
            'categories',
            'link',
            'city',
            'address',
            'timing',
            'description',
            'isDraft',
            'user'
        ]
        widgets = {
            'image_preview': forms.HiddenInput(),
            'image_original': forms.HiddenInput(),
            'name': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'categories': forms.SelectMultiple(attrs={
                'class': 'form-control select2',
            }),
            'link': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'city': forms.Select(attrs={
                'class': 'form-control select2',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'timing': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'description': forms.Textarea(attrs={
                'rows': 10,
                'class': 'form-control',
            }),
            'isDraft': forms.CheckboxInput(attrs={
                'class': 'minimal',
                'value': '1',
            }),
        }

    def clean_image_original(self):
        file_name = self.cleaned_data['image_original']
        file = Path(BASE_DIR+file_name)
        if not file.is_file():
            raise forms.ValidationError(self.error_messages['non_exist_file'], code='non_exist_file')
        return file_name

    def clean_image_preview(self):
        file_name = self.cleaned_data['image_preview']
        file = Path(BASE_DIR+file_name)
        if not file.is_file():
            raise forms.ValidationError(self.error_messages['non_exist_file'], code='non_exist_file')
        return file_name

    def clean_categories(self):
        categories = self.cleaned_data['categories']
        if len(categories) > 3:
            raise forms.ValidationError(self.error_messages['more_categories'], code='more_categories')
        return categories

    def clean(self):
        cleaned_data = super().clean()
        if abs(cleaned_data.get('x1')-cleaned_data.get('x2')) > 300 or abs(cleaned_data.get('y1')-cleaned_data.get('y2')) > 300:
            raise forms.ValidationError({'x1': [self.error_messages['wrong_coords'], ]})
