# Generated by Django 2.0 on 2018-02-10 15:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0009_eventcategory_hidden'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventDates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('meeting_date', models.DateField(verbose_name='Дата мероприятия')),
                ('time_begin', models.TimeField(verbose_name='Время начала')),
                ('event', models.ForeignKey(on_delete=False, to='event.Event')),
            ],
            options={
                'db_table': 'event_dates',
            },
        ),
    ]
