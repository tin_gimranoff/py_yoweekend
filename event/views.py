# -*- coding: UTF-8 -*-

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, Http404
from django.utils.html import linebreaks
from django.db.models.functions import datetime
from time import time
from PIL import Image

from city.models import City
from event.forms import FilterForm, AddEventForm
from event.models import Event, EventCategory, EventDate
from app.views import getPreviewSize
from yoweekend.settings import EVENTS_PER_PAGE, SUPPORT_EMAIL, EMAIL_HOST_USER, BASE_DIR, LOCALE_MONTH, SESSION_DOMAIN

import re
import os
from django.utils import dateformat

def redirect_from_old_urls(request, id = None, category_alias = None):
    if id is None and category_alias is None:
        response = redirect('/events/')
    elif id is None and category_alias is not None:
        response = redirect('/events/%s/' % category_alias)
    elif id is not None and category_alias is None:
        response = redirect('/events/event_%s/' % id)
    response.status_code = 301
    return response

def index(request, id=None, category_alias=None):
    city = City.detect_city(City, request=request)
    filter_form = FilterForm(request=request)
    filter_form.is_valid()
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    now_only_date = datetime.datetime.now().strftime('%Y-%m-%d')
    events = Event.objects.filter(city_id=city.id, isDraft=0)
    if category_alias is None:
        if filter_form.cleaned_data['meeting_date'] == '' and (
                filter_form.cleaned_data['time_begin'] == '00:00' or filter_form.cleaned_data['time_begin'] == ''):
            events = events.extra(where=[
                'concat(meeting_date, " ", time_begin) >= "{0}" OR (meeting_date >= "{1}" AND no_time = 1)'.format(
                    now, now_only_date)])
        elif filter_form.cleaned_data['meeting_date'] == '' and filter_form.cleaned_data['time_begin'] != '' and \
                filter_form.cleaned_data['time_begin'] != '00:00':
            events = events.filter(meeting_date__gte=datetime.datetime.now().strftime('%Y-%m-%d'))
            events = events.filter(time_begin__gte=filter_form.cleaned_data['time_begin'])
        elif filter_form.cleaned_data['meeting_date'] != '':
            events = events.filter(meeting_date__gte=filter_form.cleaned_data['meeting_date'])
            if filter_form.cleaned_data['time_begin'] != '' and filter_form != '00:00':
                events = events.filter(time_begin__gte=filter_form.cleaned_data['time_begin'])
        if filter_form.cleaned_data['category'] != []:
            events = events.filter(categories__id__in=filter_form.cleaned_data['category'])

    else:
        events = events.filter(categories__alias=category_alias)
        try:
            category = EventCategory.objects.get(alias=category_alias)
            category_id = category.id
        except EventCategory.DoesNotExist:
            raise Http404("Категория не найдена")
        filter_form.cleaned_data['category'] = [category_id]
        filter_form.cleaned_data['meeting_date'] = ''
        filter_form.cleaned_data['time_begin'] = ''

    events = events.order_by('meeting_date', 'time_begin').all()[:EVENTS_PER_PAGE]
    event_info = None
    event_info_text = None
    if id is not None:
        event_info = Event.objects.get(id=id)
        if event_info:
            event_info_text = re.sub(
                r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w_-]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)',
                r'<a href="\1" target="_blank">\1</a>', event_info.description, flags=re.IGNORECASE)
            event_info_text = re.sub(r'<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"', r'<a href="mailto:\1"',
                                     event_info_text)
            event_info_text = linebreaks(event_info_text)

    #Определяем метатеги
    meta_tags = {}
    if category_alias is None and id is None:
        meta_tags['title'] = u'Афиша мероприятий вашего города — %s — YOweekend.ru' % city.name
        meta_tags['description'] = u'Афиша предстоящих мероприятий в городе %s' % city.name
        meta_tags['keywords'] = u'афиша мероприятий, мероприятия, события, досуг, куда сходить, %s' % city.name

    if category_alias is not None:
        meta_tags['title'] = u'%s — Афиша мероприятий вашего города — %s — YOweekend.ru' % (category.name, city.name)
        if category.meta_description is not None:
            meta_tags['description'] = category.meta_description % (city.name)
        else:
            meta_tags['description'] = u'Афиша предстоящих мероприятий в городе %s в категории %s' % (city.name, category.name)
        if category.meta_keywords is not None:
            meta_tags['keywords'] = category.meta_keywords % (city.name)
        else:
            meta_tags['keywords'] = u'афиша мероприятий, мероприятия, события, досуг, куда сходить, %s, %s' % (city.name, category.name)

    if id is not None:
        meta_tags['title'] = u'%s — %s — Афиша мероприятий вашего города — %s — YOweekend.ru' % (event_info.name, dateformat.format(event_info.meeting_date, 'd E Y'), city.name)
        meta_tags['description'] = event_info.description[:100]
        meta_tags['keywords'] = event_info.name

    response = render(request, 'event/index.html', {
        'city': city,
        'events': events,
        'event_info': event_info,
        'event_info_text': event_info_text,
        'filter_form': filter_form.cleaned_data,
        'meta_tags': meta_tags,
    })

    if request.method == 'POST' and filter_form.is_valid():
        response.set_cookie('category', filter_form.cleaned_data['category'], domain=SESSION_DOMAIN)
        response.set_cookie('meeting_date', filter_form.cleaned_data['meeting_date'], domain=SESSION_DOMAIN)
        response.set_cookie('time_begin', filter_form.cleaned_data['time_begin'], domain=SESSION_DOMAIN)
    return response


@csrf_exempt
def get_full_info(request):
    event_id = request.POST.get('id', None)
    if event_id is None:
        pass
    data = Event.objects.get(id=event_id)
    if data is None:
        return JsonResponse({'name': u'Произошла ошибка', 'html': u'Запрашиваемое событие не найдено. Извинине.'})
    html = re.sub(
        r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w_-]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)',
        r'<a href="\1" target="_blank">\1</a>', data.description, flags=re.IGNORECASE)
    html = re.sub(r'<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"', r'<a href="mailto:\1"', html)
    return JsonResponse({'name': data.name, 'html': linebreaks(html)})


@csrf_exempt
def get_full_info_modal(request):
    event_id = request.POST.get('id', None)
    if event_id is None:
        pass
    data = Event.objects.get(id=event_id)
    if data is None:
        return JsonResponse({'name': u'Произошла ошибка', 'html': u'Запрашиваемое событие не найдено. Извинине.'})
    html = re.sub(
        r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w_-]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)',
        r'<a href="\1" target="_blank">\1</a>', data.description, flags=re.IGNORECASE)
    html = re.sub(r'<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"', r'<a href="mailto:\1"', html)
    t = get_template('event/_full_info_modal.html')
    html_to_modal = t.render({
        'data': data,
        'text': linebreaks(html),
        'request': request
    })
    return JsonResponse({
        'name': data.name,
        'html': html_to_modal,
    })


@csrf_exempt
def get_event_content(request):
    city = City.detect_city(City, request=request)
    limit = int(request.POST.get('startFrom', 0))
    offset = int(request.POST.get('startFrom', 0)) + int(EVENTS_PER_PAGE)
    filter_form = FilterForm(request.POST.get('filterData', {'category': [], 'meeting_date': '', 'time_begin': ''}))
    filter_form.is_valid()
    events = Event.objects.filter(city_id=city.id, isDraft=0)
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    now_only_date = datetime.datetime.now().strftime('%Y-%m-%d')
    if filter_form.cleaned_data['meeting_date'] == '' and (
            filter_form.cleaned_data['time_begin'] == '00:00' or filter_form.cleaned_data['time_begin'] == ''):
        events = events.extra(where=[
            'concat(meeting_date, " ", time_begin) >= "{0}" OR (meeting_date >= "{1}" AND no_time = 1)'.format(now, now_only_date)])
    elif filter_form.cleaned_data['meeting_date'] == '' and filter_form.cleaned_data['time_begin'] != '' and \
            filter_form.cleaned_data['time_begin'] != '00:00':
        events = events.filter(meeting_date__gte=datetime.datetime.now().strftime('%Y-%m-%d'))
        events = events.filter(time_begin__gte=filter_form.cleaned_data['time_begin'])
    elif filter_form.cleaned_data['meeting_date'] != '':
        events = events.filter(meeting_date__gte=filter_form.cleaned_data['meeting_date'])
        if filter_form.cleaned_data['time_begin'] != '' and filter_form != '00:00':
            events = events.filter(time_begin__gte=filter_form.cleaned_data['time_begin'])
    if filter_form.cleaned_data['category'] != []:
        events = events.filter(categories__id__in=filter_form.cleaned_data['category'])
    events = events.order_by('meeting_date', 'time_begin').all()[limit:offset]
    for e in events:
        print(e.meeting_date)
    date_from_str = request.POST.get('lastDate', None)
    if date_from_str is not None:
        date_from_str = date_from_str.split(' ')
        date_from_str = '%s-%s-%s' % (date_from_str[2], LOCALE_MONTH[date_from_str[1]], date_from_str[0])
        date_from_str = datetime.datetime.strptime(date_from_str, '%Y-%m-%d').date()
    return render(request, 'event/events.html', {
        'events': events,
        'city': city,
        'date': date_from_str,
    })


def add(request, id=None):
    if not request.user.is_authenticated:
        return redirect('/')
    if id is None:
        title = u'Новое мероприятие'
    else:
        title = u'Редактирование мероприятия'
    flash_message = {}
    model = None
    if request.method == 'POST':
        user = User.objects.get(id=request.user.id)
        if id is not None:
            try:
                model = Event.objects.get(id=id, user_id=user.id)
            except:
                return redirect('/')
            form = AddEventForm(request.POST or None, instance=model)
        else:
            form = AddEventForm(request.POST or None)
        if form.is_valid():
            event_cont = form.save(commit=False)
            # Создаем папку для изображений
            directory = '/static/media/userimages/%s/' % user.id
            if not os.path.exists(BASE_DIR+directory):
                os.makedirs(BASE_DIR+directory)
            # Копируем основное изображение из временной папки
            source_filename, file_extension = os.path.splitext(form.cleaned_data['image_original'])
            _new_file_name = hex(int(time() * 10000000))[2:]
            _main_image = Image.open(BASE_DIR+form.cleaned_data['image_original'])
            _main_image.save(BASE_DIR + directory + _new_file_name + file_extension)
            event_cont.image_original = directory + _new_file_name + file_extension
            # КОНЕЦ скопировали основное изображение
            # Создаем превью 300 на 300 из превью, которое пришло из формы
            x1 = form.cleaned_data['x1']
            y1 = form.cleaned_data['y1']
            _preview_image = Image.open(BASE_DIR+form.cleaned_data['image_preview'])
            _preview_image = _preview_image.crop((x1, y1, x1 + 300, y1 + 300))
            _preview_image.save(BASE_DIR + directory + 'preview_' + _new_file_name + file_extension)
            event_cont.image_preview = directory + 'preview_' + _new_file_name + file_extension
            # КОНЕЦ сделали превью и скопировали его в нужную папку
            event_cont.user = user
            if event_cont.id is None:
                event_cont.create_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            else:
                event_cont.update_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            event_cont.save()
            event_cont.categories.clear()
            for c in form.cleaned_data['categories']:
                event_cont.categories.add(c)
            #Дополнительные даты мероприятия
            additional_dates = request.POST.getlist('AdditionalDate__meeting_date', None)
            additional_times = request.POST.getlist('AdditionalDate__time_begin', None)
            i=0
            EventDate.objects.filter(event=event_cont).delete()
            for value in additional_dates:
                additional_date = EventDate(meeting_date=value, time_begin=additional_times[i] or None, event=event_cont)
                additional_date.save()
                i=i+1
            messages.add_message(request, messages.SUCCESS,
                                 'Ваше мероприятия успешно сохранено, теперь его видят пользователи в ленте.',
                                 extra_tags='SAVE_SUCCESS')
            return redirect('/event/edit/%s/' % event_cont.id)
        else:
            flash_message = {
                'class': 'warning',
                'title': 'Ошибка',
                'message': 'В форме есть ошибки. Исправьте их и повторите сохранение',
            }
    else:
        if id is None:
            form = AddEventForm(initial={
                'image_preview': '/static/img/temp_image.png',
                'image_original': '/static/img/temp_image.png',
                'x1': 0,
                'x2': 299,
                'y1': 0,
                'y2': 299,
            })
        else:
            try:
                model = Event.objects.get(id=id, user_id=request.user.id)
            except:
                return redirect('/')
            form = AddEventForm(initial={
                'x1': 0,
                'x2': 299,
                'y1': 0,
                'y2': 299,
            }, instance=model)
    return render(request, 'event/add.html', {
        'form': form,
        'flash_message': flash_message,
        'title': title,
        'model': model
    })


def upload_image(request):
    if request.is_ajax() and request.FILES['event_image']:
        image_file = request.FILES['event_image']
        source_filename, file_extension = os.path.splitext(image_file.name)
        if file_extension not in ['.jpg', '.png', '.jpeg']:
            return JsonResponse({
                'status': False,
                'textError': u'Вы можете загружать только файлы изображений с разрешениями png, jpg, jpeg.'
            })
        fs = FileSystemStorage()
        directory = '/static/media/temp/%s/' % (request.session.session_key)
        if not os.path.exists(BASE_DIR+directory):
            os.makedirs(BASE_DIR+directory)
        uid = hex(int(time() * 10000000))[2:]
        target_file_name = uid + file_extension
        target_uploaded_file_name = fs.save(BASE_DIR+directory + target_file_name, image_file)
        target_uploaded_file_path = fs.url(target_uploaded_file_name)
        if target_uploaded_file_path:
            source_image = Image.open('/'+target_uploaded_file_path)
            width, height = source_image.size
            options = getPreviewSize(width, height)
            preview_image = source_image.resize((options['width'], options['height']), Image.ANTIALIAS)
            preview_image.save(BASE_DIR+directory + 'preview_' + target_file_name)
            target_uploaded_file_path_preview = directory + 'preview_' + target_file_name
            return JsonResponse({
                'name': target_file_name,
                'size': image_file.size,
                'url': directory + target_file_name,
                'thumbnailUrl': target_uploaded_file_path_preview,
                'width': options['width'],
                'height': options['height'],
            })


def usr_events(request, is_draft=None):
    if not request.user.is_authenticated:
        return redirect('/')
    events = Event.objects.filter(user_id=request.user.id)
    if is_draft != None and is_draft == 'drafts':
        events = events.filter(isDraft=True)
    events = events.order_by('-meeting_date', '-time_begin').all()

    return render(request, 'event/user_events.html', {
        'events': events,
        'is_draft': is_draft,
    })


@csrf_exempt
def delete(request):
    if not request.is_ajax():
        return JsonResponse({
            'status': False,
            'error': u'Это не ajax запрос',
        })
    id = request.POST.get('id', None)
    if id is None:
        return JsonResponse({
            'status': False,
            'error': u'Не верный запрос.',
        })
    if request.method == 'POST':
        event = Event.objects.get(id=id)
        if not event:
            return JsonResponse({
                'status': False,
                'error': u'По какой-то причине данного мероприятия не существует.',
            })
        if event.user_id != request.user.id:
            return JsonResponse({
                'status': False,
                'error': u'Данное мероприятие принадлежит не вам.',
            })
        EventDate.objects.filter(event=event).delete()
        if event.delete():
            return JsonResponse({
                'status': True,
            })
        else:
            return JsonResponse({
                'status': False,
                'error': u'В процессе удаления произошла ошибка.',
            })

@csrf_exempt
def send_new_category(request):
    if not request.is_ajax():
        return JsonResponse({
            'status': False,
            'textMessage': u'Это не AJAX запрос.',
        })
    category_name = request.POST.get('category_name', None)
    if category_name is None:
        return JsonResponse({
            'status': False,
            'textMessage': u'Вы не указали название категории.',
        })
    if send_mail(u'Запрос на добавление категории', u'Запрос на добавление категории: %s' % category_name, EMAIL_HOST_USER, [SUPPORT_EMAIL], fail_silently=False):
        return JsonResponse({
            'status': True,
            'textMessage': u'Ваша заявка отправлена, вы можете пока сохранить свое мероприятие, как черновик. После добавления новой категории мы с вами свяжемся.',
        })
    else:
        return JsonResponse({
            'status': False,
            'textMessage': u'В процессе отправки сообщения произошла ошибка. Повторите запрос позднее.',
        })

@csrf_exempt
def get_event_info_from_vk(request):
    if not request.is_ajax():
        return JsonResponse({
            'status': False,
            'textMessage': u'Это не AJAX запрос.',
        })
    link = request.POST.get('link', None)
    if link is None or link == '':
        return JsonResponse({
            'status': False,
            'textMessage': u'Ссылка пуста. Пожалуйста введите ссылку',
        })
    from yoweekend.settings import VK_LOGIN, VK_PASSWORD
    import vk_api
    vk_session = vk_api.VkApi(VK_LOGIN, VK_PASSWORD)
    try:
        vk_session.auth()
    except vk_api.AuthError:
        return JsonResponse({
            'status': False,
            'textMessage': u'Не удалось подключиться к сайту vk.com.',
        })
    parse_url = link.split('/')
    group_id = parse_url[len(parse_url)-1]
    if re.search('^event[0-9]+$', group_id, flags=re.IGNORECASE):
        group_id = group_id.replace('event', '')
    if re.search('^club[0-9]+$', group_id, flags=re.IGNORECASE):
        group_id = group_id.replace('club', '')
    vk = vk_session.get_api()
    try:
        group_info = vk.groups.getById(group_id=group_id, fields='city,place,activity,description')
        arg = {
            'status': True,
            'name': group_info[0]['name'],
            'description': group_info[0]['description']
        }
        try:
            import requests
            from io import BytesIO
            photos = vk.photos.get(owner_id='-%s' % group_info[0]['id'], album_id='profile', rev=1, photo_sizes=1)
            max_size = None
            photo_url = None
            for photo in photos['items'][0]['sizes']:
                if max_size == None or max_size < photo['width']:
                    max_size = photo['width']
                    photo_url = photo['src']
            directory = '/static/media/temp/%s/' % (request.session.session_key)
            if not os.path.exists(BASE_DIR + directory):
                os.makedirs(BASE_DIR + directory)

            source_filename, file_extension = os.path.splitext(photo_url)
            uid = hex(int(time() * 10000000))[2:]
            target_file_name = uid + file_extension
            response_image = requests.get(photo_url)
            image = Image.open(BytesIO(response_image.content))
            image.save(BASE_DIR + directory + target_file_name)
            width, height = image.size
            options = getPreviewSize(width, height)
            preview_image = image.resize((options['width'], options['height']), Image.ANTIALIAS)
            preview_image.save(BASE_DIR + directory + 'preview_' + target_file_name)
            arg['photo_original'] = directory + target_file_name
            arg['photo_preview'] = directory + 'preview_' + target_file_name
            arg['photo_width'] = options['width']
            arg['photo_height'] = options['height']
        except:
            arg['photo'] = '/static/img/temp_image.png'

        try:
            city = City.objects.get(name=group_info[0]['city']['title'])
            if city:
                arg['city'] = city.name
            else:
                city = City.objects.get(default=True)
                arg['city'] = city.name
        except:
            city = City.objects.get(default=True)
            arg['city'] = city.name

        try:
            arg['address'] = group_info[0]['place']['address']
        except:
            arg['address'] = ''

        try:
            import locale
            locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
            date = datetime.datetime.strptime(group_info[0]['activity'], '%d %b %Y в %H:%M')
            arg['date'] = datetime.datetime.strftime(date, '%Y-%m-%d')
            arg['time'] = datetime.datetime.strftime(date, '%H:%M')
        except:
            arg['date'] = arg['time'] = ''

        return JsonResponse(arg)
    except:
        return JsonResponse({
            'status': False,
            'textMessage': u'Вы ввели неверную ссылку или ссылка не является группой или мероприятием в ВК.',
        })
