from django.urls import path
from event import views

urlpatterns = [
    path('', views.index, name='event_list'),
    path('event_<int:id>/', views.index),
    path('<str:category_alias>/', views.index)
]
