from django.contrib import admin

from event.models import EventCategory, Event


class EventCategoryAdmin(admin.ModelAdmin):
    def category_event_count(self, obj):
        return obj.event_set.count()
    category_event_count.short_description = u'Количество мероприятий'
    list_display = ('name', 'hidden', 'alias', 'category_event_count', 'meta_keywords')


class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'meeting_date', 'time_begin', 'isVkSend', 'user')
    list_filter = ('user',)


admin.site.register(EventCategory, EventCategoryAdmin)
admin.site.register(Event, EventAdmin)
