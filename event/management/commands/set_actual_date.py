from django.core.management.base import BaseCommand
from datetime import datetime

from event.models import Event


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('no_time', type=int)

    def handle(self, *args, **options):
        if options['no_time'] == 0:
            current_time = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            events = Event.objects.extra(where=[
                'concat(meeting_date, " ", time_begin) < "{0}" AND no_time=0'.format(current_time)
            ]).all()
        elif options['no_time'] == 1:
            current_time = datetime.today().strftime('%Y-%m-%d 23:59:00')
            events = Event.objects.extra(where=[
                'concat(meeting_date, " ", time_begin) < "{0}" AND no_time=1'.format(current_time)
            ]).all()
        for event in events:
            additional_dates = event.additions_event_dates(current_time)
            if additional_dates:
                event.meeting_date = additional_dates[0].meeting_date
                event.time_begin = additional_dates[0].time_begin
                event.save()
