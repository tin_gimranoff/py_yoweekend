from django import template

from event.forms import FilterForm
from event.models import EventCategory
from django.db.models.functions import datetime
from django.utils.dateparse import parse_date
import locale

register = template.Library()


@register.inclusion_tag('filter/modal_filter_form.html')
def modal_filter_form(request):
    filter_form = FilterForm(request)
    return {
        'filter_form': filter_form,
        'request': request
    }

@register.inclusion_tag('filter/sidebar_filter_form.html')
def sidebar_filter_form(request):
    filter_form = FilterForm(request)
    return {
        'filter_form': filter_form,
        'request': request
    }

@register.inclusion_tag('filter/_active_filter_panel.html')
def active_filter_panel(filter_form):
    locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    categories = tuple((c.name) for c in EventCategory.objects.filter(id__in=filter_form['category']).order_by('position', 'name').all())
    if categories != ():
        categories_str = ', '.join(categories)
    else:
        categories_str = u'Все категории'
    if filter_form['meeting_date'] != '' and filter_form['meeting_date'] is not None:
        meeting_data = parse_date(filter_form['meeting_date']).strftime('%d %B %Y')
    else:
        meeting_data = datetime.datetime.now().strftime('%d %B %Y')

    if filter_form['time_begin'] == '00:00' or filter_form['time_begin'] == '' or filter_form['time_begin'] is None:
        time_begin = 'Не важно'
    else:
        time_begin = filter_form['time_begin']
    return {
        'categories_str': categories_str,
        'meeting_data': meeting_data,
        'time_begin': time_begin,
    }

