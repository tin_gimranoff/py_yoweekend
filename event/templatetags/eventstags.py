from django import template
from django.contrib import auth
from event.models import EventDate

register = template.Library()


@register.inclusion_tag('event/_add_event_btn.html')
def add_event_button(request):
    argv = {};
    if(auth.get_user(request).is_authenticated == False):
        argv['onclick_title'] = u'Требуется авторизация'
        argv['onclick_message'] = u'Для того что бы доавблять свои мероприятия вам необходимо <a href="/usr/login/">войти</a> или <a href="/usr/signup/">зарегистрироваться</a>.'

    return {
        'argv': argv,
        'user_auth': auth.get_user(request).is_authenticated
    }

@register.inclusion_tag('event/_addition_dates.html')
def get_adition_dates_template(model):
    addition_dates = EventDate.objects.filter(event=model).order_by('meeting_date', 'time_begin')
    if len(addition_dates) != 0:
        show = 'block'
    else:
        show = 'none'
    return {
        'dates': addition_dates,
        'show': show,
    }