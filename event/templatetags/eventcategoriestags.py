from event.models import EventCategory
from django import template

register = template.Library()


@register.inclusion_tag('categories/_list_in_sidebar.html')
def sidebarlist(request):
    categories = EventCategory.objects.order_by('position', 'name').filter(hidden=False).all()
    return {
        'categories': categories,
        'request': request
    }
