from django.db import models
from django.contrib.auth.models import User
from yoweekend.settings import VK_BOT_LOGIN, VK_BOT_PASSWORD, VK_USER_IDENT, VK_MODERATOR_NAME, VK_MODERATOR_ID
from city.models import City
from datetime import datetime
import vk_api

translated_months = {
    '01': 'января',
    '02': 'февраля',
    '03': 'марта',
    '04': 'апреля',
    '05': 'мая',
    '06': 'июня',
    '07': 'июля',
    '08': 'августа',
    '09': 'сентября',
    '10': 'октября',
    '11': 'ноября',
    '12': 'декабря',
}


class EventCategory(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'event_categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('position', 'name')

    name = models.CharField(max_length=255, verbose_name=u'Название', null=False, blank=False, unique=True)
    alias = models.CharField(max_length=255, verbose_name=u'Алиас', null=False, blank=False, unique=True)
    meta_keywords = models.CharField(max_length=255, verbose_name=u'Ключевые слова', null=True, blank=True)
    meta_description = models.CharField(max_length=255, verbose_name=u'Описание', null=True, blank=True)
    position = models.IntegerField(default=0, verbose_name=u'Положение')
    hidden = models.BooleanField(default=False, verbose_name=u"Скрыть категорию")


class EventDate(models.Model):
    class Meta:
        db_table = 'event_dates'

    meeting_date = models.DateField(null=False, blank=False, verbose_name=u'Дата мероприятия')
    time_begin = models.TimeField(null=False, blank=False, verbose_name=u'Время начала')
    event = models.ForeignKey('Event', on_delete=False, blank=False, null=False)


class Event(models.Model):
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'events'
        verbose_name = 'Событие'
        verbose_name_plural = 'События'

    name = models.CharField(max_length=255, verbose_name=u'Название мероприятия', null=False, blank=False)
    image_original = models.CharField(max_length=255, verbose_name=u'Оригинальное изображение', null=True, blank=True,
                                      default=None)
    image_preview = models.CharField(max_length=255, verbose_name=u'Оригинальное изображение', null=True, blank=True,
                                     default=None)
    link = models.CharField(max_length=255, verbose_name=u'Ссылка на мероприятие', null=True, blank=True, default=None)
    city = models.ForeignKey(City, verbose_name=u'Город', on_delete=None, null=False, blank=False, default='')
    categories = models.ManyToManyField(EventCategory, verbose_name=u'Категории', default=None)
    address = models.CharField(max_length=255, verbose_name=u'Адрес', null=False, blank=False)
    meeting_date = models.DateField(null=False, blank=False, verbose_name=u'Дата мероприятия')
    time_begin = models.TimeField(null=False, blank=False, verbose_name=u'Время начала')
    no_time = models.BooleanField(default=False, verbose_name=u'Не показывать время (не рекомендуется)')
    price = models.IntegerField(default=None, null=True, blank=True, verbose_name=u'Стоимость посещения')
    description = models.TextField(null=True, blank=True, verbose_name=u'Описание', default=None)
    isDraft = models.BooleanField(default=False, verbose_name=u'Черновик')
    isVkSend = models.BooleanField(default=False, verbose_name=u'Статус отправки в группу в ВК')
    user = models.ForeignKey(User, on_delete=None, blank=True)
    create_date = models.DateTimeField(verbose_name=u'Дата создания', default=None, null=True)
    update_date = models.DateTimeField(verbose_name=u'Дата обновления', default=None, null=True)

    def save(self, *args, **kwargs):
        if self.pk is None:
            is_new_record = True
        else:
            is_new_record = False
        super(Event, self).save(*args, **kwargs)
        if self.user.username == VK_MODERATOR_NAME:
            return
        login, password = VK_BOT_LOGIN, VK_BOT_PASSWORD
        try:
            vk_session = vk_api.VkApi(login, password)
            vk_session.auth()
        except vk_api.AuthError:
            return
        message = 'В системе %s мероприятие\n' % (u'новое' if is_new_record == True else u'обновилось')
        message = message + u'Номер в системе #%s\n' % self.pk
        message = message + u'Название: %s\n' % self.name
        message = message + u'От пользователя: %s\n' % self.user.username
        message = message + u'Описание:\n%s' % self.description
        vk = vk_session.get_api()
        vk.messages.send(user_id=VK_USER_IDENT, message=message)
        vk.messages.send(user_id=VK_MODERATOR_ID, message=message)

    def additions_event_dates(self, current_date):
        dates = EventDate.objects.filter(event=self).extra(where=[
            'concat(meeting_date, " ", time_begin) >= "{0}"'.format(current_date)
        ]).order_by('meeting_date', 'time_begin').all()
        return dates

    def get_all_dates_string(self):
        dates = EventDate.objects.filter(event=self).order_by('meeting_date', 'time_begin').all()
        dates_arg = {}
        result_date_list = []
        if len(dates) < 5 and self.no_time == False:
            for d in dates:
                month = d.meeting_date.strftime('%m')
                day = d.meeting_date.strftime('%d')
                time = d.time_begin.strftime('%H:%M')
                if dates_arg.get(month, None) is None:
                    dates_arg[month] = {}
                if dates_arg[month].get(time, None) is None:
                    dates_arg[month][time] = []
                dates_arg[month][time].append(day)
            month = self.meeting_date.strftime('%m')
            day = self.meeting_date.strftime('%d')
            time = self.time_begin.strftime('%H:%M')
            if dates_arg.get(month, None) is None:
                dates_arg[month] = {}
            if dates_arg[month].get(time, None) is None:
                dates_arg[month][time] = []
            dates_arg[month][time].append(day)
            dates_arg = dict(sorted(dates_arg.items()))
            for month, times in dates_arg.items():
                times = dict(sorted(times.items()))
                for time, days in times.items():
                    days_list_sorted = sorted(list(set(days)))
                    result_date_list.append(
                        '%s %s в %s' % (', '.join(days_list_sorted), translated_months[month], time))
            result_date_list = '; '.join(result_date_list)
        if len(dates) < 5 and self.no_time == True:
            for d in dates:
                month = d.meeting_date.strftime('%m')
                day = d.meeting_date.strftime('%d')
                if dates_arg.get(month, None) is None:
                    dates_arg[month] = []
                dates_arg[month].append(day)
            month = self.meeting_date.strftime('%m')
            day = self.meeting_date.strftime('%d')
            if dates_arg.get(month, None) is None:
                dates_arg[month] = []
            dates_arg[month].append(day)
            dates_arg = dict(sorted(dates_arg.items()))
            for month, days in dates_arg.items():
                days_list_sorted = sorted(list(set(days)))
                result_date_list.append(
                    '%s %s' % (', '.join(days_list_sorted), translated_months[month]))
            result_date_list = '; '.join(result_date_list)
        if len(dates) >= 5:
            first_date = dates.first().meeting_date.strftime("%Y-%m-%d") + ' ' + dates.first().time_begin.strftime("%H:%M")
            last_date = dates.last().meeting_date.strftime("%Y-%m-%d") + ' ' + dates.last().time_begin.strftime("%H:%M")
            main_date = self.meeting_date.strftime("%Y-%m-%d") + ' ' + self.time_begin.strftime("%H:%M")
            if main_date < first_date:
                first_date = main_date
            if main_date > last_date:
                last_date = main_date
            if self.no_time == True:
                result_date_list = u'с %s %s по %s %s' % (
                    datetime.strptime(first_date, '%Y-%m-%d %H:%M').strftime('%d'),
                    translated_months[datetime.strptime(first_date, '%Y-%m-%d %H:%M').strftime('%m')],
                    datetime.strptime(last_date, '%Y-%m-%d %H:%M').strftime('%d'),
                    translated_months[datetime.strptime(last_date, '%Y-%m-%d %H:%M').strftime('%m')]
                )
            else:
                result_date_list = u'с %s %s по %s %s с %s' % (
                    datetime.strptime(first_date, '%Y-%m-%d %H:%M').strftime('%d'),
                    translated_months[datetime.strptime(first_date, '%Y-%m-%d %H:%M').strftime('%m')],
                    datetime.strptime(last_date, '%Y-%m-%d %H:%M').strftime('%d'),
                    translated_months[datetime.strptime(last_date, '%Y-%m-%d %H:%M').strftime('%m')],
                    datetime.strptime(first_date, '%Y-%m-%d %H:%M').strftime('%H:%M')
                )
        if len(dates) == 0:
            if self.no_time == 1:
                time = ''
            else:
                time = ' в '+self.time_begin.strftime("%H:%M")
            result_date_list = self.meeting_date.strftime("%d") + ' ' + translated_months[self.meeting_date.strftime("%m")] + time
        return result_date_list
