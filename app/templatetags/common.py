from PIL import Image
from django import template

from yoweekend.settings import BASE_DIR

register = template.Library()


class SetVarNode(template.Node):

    def __init__(self, var_name, var_value):
        self.var_name = var_name
        self.var_value = var_value

    def render(self, context):
        try:
            value = template.Variable(self.var_value).resolve(context)
        except template.VariableDoesNotExist:
            value = ""
        context[self.var_name] = value

        return u""


@register.tag(name='set')
def set_var(parser, token):
    """
    {% set some_var = '123' %}
    """
    parts = token.split_contents()
    if len(parts) < 4:
        raise template.TemplateSyntaxError("'set' tag must be of the form: {% set <var_name> = <var_value> %}")

    return SetVarNode(parts[1], parts[3])


@register.filter(name='getimagesize')
def getimagezise(image_url, arg):
    source_image = Image.open(BASE_DIR + image_url)
    width, height = source_image.size
    if arg == 'width':
        return width
    else:
        return height


@register.inclusion_tag('menu/_select_mode_menu.html', takes_context=True)
def get_mode_menu(context):
    path = context['request'].path
    mode = u'Режим'
    if '/event/' in path or '/events/' in path:
        mode = u'Мероприятия'
    elif '/places/' in path:
        mode = u'Места'
    return {
        'mode': mode
    }
