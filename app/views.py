from django.shortcuts import render


def getPreviewSize(width, height):
    ratio = width/height
    options = {}
    if width > height:
        options['height'] = 300
        options['width'] = round(300*ratio)
    elif height > width:
        options['width'] = 300
        options['height'] = round(300/ratio)
    else:
        options['width'] = 300
        options['height'] = 300

    return options

def handler404(request, exception, template_name='404.html'):
    respone = render(request, template_name)
    respone.status_code = 404
    return respone

def handler500(request, template_name='500.html'):
    response = render(request, template_name)
    response.status_code = 500
    return response