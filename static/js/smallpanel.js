$(document).ready(function () {
    var isMobile = {
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            }
    }
    if(isMobile.iOS()) {
        $(".main-sidebar").css({'position':'absolute'});
    }
    //Замена верхнего меню на компактное и дату
    $(window).scroll(function () {
        if(isMobile.iOS())
            return false;
        var top = $('.main-header').offset().top,
            sctop = $(this).scrollTop(),
            winh = $(this).height(),
            y = top - sctop - winh;
        if ($(this).width() >= 768)
            var asidePaddingTop = 50;
        else
            var asidePaddingTop = 100;
        if (y > 0 || -y > winh) {
            $('.main-header').hide();
            $('.main-header-compact').show();
            //if ($('body').hasClass('sidebar-collapse') == false) {
                $('.main-sidebar').css('padding-top', 30);
            //}
        }
        else {
            $('.main-header').show();
            $('.main-header-compact').hide();
            //if ($('body').hasClass('sidebar-collapse') == false) {
                $('.main-sidebar').css('padding-top', asidePaddingTop);
            //}
        }
    });
    //Подстраиваем левую панель под высоту скролла
    //Для маленького меню
    $(".sidebar-toggle-compact").bind('click', function () {
        if(!isMobile.iOS()) {
            if ($('body').hasClass('sidebar-collapse') == true) {
                $('.main-sidebar').css('padding-top', 30);
            }
        }
    });
    //Для большого меню
    $(".sidebar-toggle").bind('click', function () {
        if(!isMobile.iOS()) {
            if ($(window).width() >= 768)
                var asidePaddingTop = 50;
            else
                var asidePaddingTop = 100;
            if ($('body').hasClass('sidebar-collapse') == true && $(this).hasClass('sidebar-toggle-compact') == false) {
                $('.main-sidebar').css('padding-top', asidePaddingTop);
            }
        }
    });
});