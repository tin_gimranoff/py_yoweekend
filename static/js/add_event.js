$(document).ready(function () {
    $("#get-event-info-from-vk").on('click', function () {
        event_url = $("#id_link").val();
        $.ajax({
            url: '/event/get-event-info-from-vk/',
            type: 'post',
            data: {link: event_url},
            dataType: 'json',
            beforeSend: function () {
                load_layout('block');
            },
            success: function (data) {
                load_layout('none');
                if (data.status == false) {
                    show_info_modal('Ошибка', data.textMessage);
                } else {
                    $(".img-responsive.object-image").imgAreaSelect( {remove: true} );
                    $(".img-responsive.object-image")
                        .attr('src', data['photo_preview'])
                        .attr('data-real-width', data['photo_width'])
                        .attr('data-real-height', data['photo_height']);
                    $("#id_image_preview").val(data['photo_preview']);
                    $("#id_image_original").val(data['photo_original']);
                    $("#event-x1").val(0);
                    $("#event-x2").val(299);
                    $("#event-y1").val(0);
                    $("#event-y2").val(299);
                    $('.img-responsive.object-image').imgAreaSelect({
                        aspectRatio: '1:1',
                        handles: true,
                        onSelectEnd: setImgCoordinates,
                        //onInit: setImgCoordinates,
                        minWidth: 299,
                        minHeight: 299,
                        maxWidth: 300,
                        maxHeight: 300,
                        show: true,
                        imageHeight: data['photo_height'],
                        imageWidth: data['photo_width'],
                        x1: 0,
                        x2: 299,
                        y1: 0,
                        y2: 299,
                    });
                    $("#id_name").val(data['name']);
                    $("#id_description").val(data['description']);
                    $("#add_event_form .timepicker").val(data['time']);
                    $('#add_event_form .datepicker-modal').datepicker('setDate', data['date']);
                    $("#id_address").val(data['address']);
                    $("#id_city").select2("val", $("#id_city option:contains("+data['city']+")").val() );
                }
            },
            error: function () {
                load_layout('none');
                show_info_modal('Ошибка', 'Произошла ошибка на сервере. Пожалуйста заполните данные вручную.')
            }
        });
        return false;
    });
    
    $("#send-new-addition-date").on('click', function(){
    		var date = $("#add-additional-dates input[name=addition_meeting_date]").val();
    		var time = $("#add-additional-dates input[name=addition_time_begin]").val();
    		if(date == '') {
    			$("#addition-meeting-date").parent().parent().addClass('has-error');
    			$("#addition_date_error").empty().append('Поле "Дата" необходимо заполнить.');
    		} else {
    			$("#addition-meeting-date").parent().parent().removeClass('has-error');
    			$("#addition_date_error").empty();
    			$(".additional-dates").append(
    				'<div class="additional-date">'+
    					'<span>'+date+' '+time+'</span><a href="#" onclick="removeAdditionDate(this); return false;"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Убрать</a>'+
    					'<input type="hidden" name="AdditionalDate__meeting_date" value="'+date+'">'+
            			'<input type="hidden" name="AdditionalDate__time_begin" value="'+time+'">'+
    				'</div>'
    			);
    			$('#add-additional-dates').modal('hide');
    			var addition_dates = $(".additional-dates").children();
			if(addition_dates.length != 0)
				$(".additional-dates").show();
    		}
    });
    
    $('#add-additional-dates').on('hidden.bs.modal', function () {
		$("#addition-meeting-date").parent().parent().removeClass('has-error');
    		$("#addition_date_error").empty();
    		$("#add-additional-dates input[name=addition_meeting_date]").val('');
    		$("#add-additional-dates input[name=addition_time_begin]").val('00:00');
	});

});

function removeAdditionDate(El) {
	$(El).parent().remove();
	var addition_dates = $(".additional-dates").children();
	if(addition_dates.length == 0)
		$(".additional-dates").hide();
	return false;
}