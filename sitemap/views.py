from django.shortcuts import render

from city.models import City
from event.models import EventCategory, Event
from place.models import PlaceCategory, Place


def sitemap(request):
    urls = []
    server_name = request.META['SERVER_NAME']
    #urls.append({
    #    'loc': 'http://%s' % (server_name),
    #    'changefreq': 'never',
    #    'priority': '0.8'
    #})
    urls.append({
        'loc': 'http://%s/events/' % (server_name),
        'changefreq': 'hourly',
        'priority': '0.8'
    })

    event_categories = EventCategory.objects.all()
    for e in event_categories:
        urls.append({
            'loc': 'http://%s/events/%s/' % (server_name, e.alias),
            'changefreq': 'hourly',
            'priority': '0.8'
        })
    events = Event.objects.all()
    for e in events:
        urls.append({
            'loc': 'http://%s/events/event_%s/' % (server_name, e.id),
            'changefreq': 'daily',
            'priority': '0.8'
        })
    #Места и категории мест
    place_categories = PlaceCategory.objects.all()
    for p in place_categories:
        urls.append({
            'loc': 'http://%s/places/%s/' % (server_name, p.alias),
            'changefreq': 'hourly',
            'priority': '0.8'
        })
    places = Place.objects.all()
    for p in places:
        urls.append({
            'loc': 'http://%s/places/place_%s/' % (server_name, p.id),
            'changefreq': 'daily',
            'priority': '0.8'
        })
    return render(request, 'sitemap.xml', {
        'urls': urls,
    }, content_type='text/xml')
