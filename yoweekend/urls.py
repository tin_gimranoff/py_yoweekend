"""yoweekend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include

import event
import place.views
from event.views import get_full_info, get_full_info_modal, get_event_content
from sitemap.views import sitemap

urlpatterns = [
    path('obninsk/', event.views.redirect_from_old_urls),
    path('obninsk/event_<int:id>/', event.views.redirect_from_old_urls),
    path('obninsk/<str:category_alias>/', event.views.redirect_from_old_urls),
    
    path('admin/', admin.site.urls),
    path('', include('city.urls')),
    path('ajax/get-full-info/', get_full_info),
    path('ajax/get-full-info-modal/', get_full_info_modal),
    path('ajax/get-event-content/', get_event_content),
    path('usr/', include('allauth.urls')),
    path('usr/my-events/<str:is_draft>/', event.views.usr_events, name='user_events_is_draft'),
    path('usr/my-events/', event.views.usr_events, name='user_events'),
    path('usr/my-places/', place.views.usr_places, name='user_places'),
    path('usr/my-places/<str:is_draft>/', place.views.usr_places, name='user_places_is_draft'),
    path('event/add/', event.views.add, name='add_event'),
    path('event/edit/<int:id>/', event.views.add, name='edit_event'),
    path('event/delete/', event.views.delete, name='delete_event'),
    path('event/upload_image', event.views.upload_image, name='upload_event_image'),
    path('event/send-new-category/', event.views.send_new_category, name='send_new_category'),
    path('event/get-event-info-from-vk/', event.views.get_event_info_from_vk),
    path('sitemap.xml', sitemap, name='sitemap'),
    path('events/', include('event.urls')),
    path('places/', include('place.urls')),
]

handler404 = 'app.views.handler404'
handler500 = 'app.views.handler500'
