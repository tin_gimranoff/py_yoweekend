from city.models import City
from django import template
from yoweekend.settings import BASE_HOST

register = template.Library()


@register.inclusion_tag('city/_city_menu.html')
def citymenu():
    cities = City.objects.filter(hidden=False).order_by('name').all()
    return {
        'cities': cities,
        'BASE_HOST': BASE_HOST,
    }

@register.simple_tag()
def detect_current_city_name(request):
    return City.detect_city(City, request).name

@register.simple_tag()
def detect_current_city_alias(request):
    return City.detect_city(City, request).alias
