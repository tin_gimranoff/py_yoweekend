from django import forms

from city.models import City


class SelectCityForm(forms.Form):
    name = forms.ChoiceField(
        choices=[(c.alias, c.name) for c in City.objects.all()],
        widget=forms.Select(attrs={
            'class':'form-control',
        }),
        required=True,
    )