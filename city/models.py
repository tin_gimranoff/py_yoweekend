from django.db import models
from django.shortcuts import get_object_or_404


class City(models.Model):
    def __str__(self):
        return self.name

    class Meta:
        db_table = "city"
        verbose_name = "Город"
        verbose_name_plural = "Города"
        ordering = ["name"]
    name = models.CharField(max_length=255, verbose_name=u'Название', null=False, blank=False)
    alias = models.CharField(max_length=255, verbose_name=u'Alias', null=False, blank=False)
    hidden = models.BooleanField(default=False, verbose_name=u'Скрытый')
    default = models.BooleanField(default=False, verbose_name=u'По умолчанию')
    
    def detect_city(self, request):
        current_host = request.META['HTTP_HOST']
        current_host = current_host.split('.')
        if current_host[0] == 'local' or current_host[0] == 'yoweekend':
            city = self.objects.get(default=True)
        else:
            city = get_object_or_404(self, alias=current_host[0])
        return city
