from django.apps import AppConfig


class CityConfig(AppConfig):
    name = 'city'
    verbose_name = 'Города'
