from django.urls import path

from city import views

urlpatterns = [
    path('', views.detect_city, name='home'),
]
