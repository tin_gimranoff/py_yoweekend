from django.contrib import admin
from city.models import City

# Register your models here.

class CityAdmin(admin.ModelAdmin):
    fields = ['name', 'alias', 'hidden', 'default']
    list_display = ('name', 'alias', 'default')


admin.site.register(City, CityAdmin)