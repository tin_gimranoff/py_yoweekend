from django.shortcuts import redirect, render

from city.forms import SelectCityForm
from city.models import City
from yoweekend.settings import BASE_HOST, SESSION_DOMAIN


def detect_city(request):
    response = redirect('http://%s/events/' % request.META['HTTP_HOST'])
    response.status_code = 301
    return response
    
    #city_default = City.objects.get(default=True)
    #if request.COOKIES.get('city'):
    #    if request.COOKIES.get('city') == city_default.alias:
    #        response = redirect('http://%s/events/' % BASE_HOST)
    #    else:
    #        response = redirect('http://%s.%s/events/' % (request.COOKIES.get('city'), BASE_HOST))
    #    return response
    #else:
    #    if request.method == 'POST':
    #        form = SelectCityForm(request.POST or None)
    #        if form.is_valid():
    #            if form.cleaned_data['name'] == city_default.alias:
    #                response = redirect('http://%s/events/' % BASE_HOST)
    #            else:
    #                response = redirect('http://%s.%s/events/' % (form.cleaned_data['name'], BASE_HOST))
    #            city = City.objects.get(alias=form.cleaned_data['name'])
    #            if city:
    #                response.set_cookie('city', city.alias, domain=SESSION_DOMAIN)
    #            return response
    #    else:
    #        form = SelectCityForm()
    #    # Определяем метатеги
    #    meta_tags = {}
    #    meta_tags['title'] = u'Афиша мероприятий  — Выбор города — YOweekend.ru'
    #    meta_tags['description'] = u'Выбор города для показа предстоящих мероприятий'
    #    meta_tags['keywords'] = u'афиша мероприятий, ваш город'
    #    response = render(request, 'mainpage.html', {
    #        'form': form,
    #        'meta_tags': meta_tags
    #    })
    #    return response
